#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "func_qry.h"



static float min(float a, float b){
    if(a < b)
        return a;
    else
        return b;
}

static float max(float a, float b){
    if(a > b)
        return a;
    else
        return b;
}

//Arquivo qry:

void InsideRect(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, float x, float y, float dim1, float dim2){
    fprintf(txt, "Buscando dentro do retângulo:\n");
    fprintf(txt, "Quadras:\n");
    while(quadras != NULL){
        if(quadras->x >= x){
            if(quadras->y >= y){
                if(quadras->x + quadras->dim1 <= x + dim1){
                    if(quadras->y + quadras->dim2 <= y + dim2){
                        //Uh, ele realmente está dentro não é mesmo?
                        fprintf(txt, "CEP: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", quadras->string, quadras->x, quadras->y, quadras->dim1, quadras->dim2, quadras->cor1, quadras->cor2);
                    }
                }
            }
        }
        quadras = quadras->next;
    }
    fprintf(txt, "Hidrantes:\n");
    while(hidrantes != NULL){
        if(hidrantes->x - hidrantes->dim1 >= x){
            if(hidrantes->y - hidrantes->dim1 >= y){
                if(hidrantes->x + hidrantes->dim1 <= x + dim1){
                    if(hidrantes->y + hidrantes->dim1 <= y + dim2){
                        //Uh, ele realmente está dentro não é mesmo?
                        fprintf(txt, "ID: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", hidrantes->string, hidrantes->x, hidrantes->y, hidrantes->dim1, hidrantes->dim2, hidrantes->cor1, hidrantes->cor2);
                    }
                }
            }
        }
        hidrantes = hidrantes->next;
    }
    fprintf(txt, "Semáforos:\n");
    while(semaforos != NULL){
        if(semaforos->x - semaforos->dim1 >= x){
            if(semaforos->y - semaforos->dim1 >= y){
                if(semaforos->x + semaforos->dim1 <= x + dim1){
                    if(semaforos->y + semaforos->dim1 <= y + dim2){
                        //Uh, ele realmente está dentro não é mesmo?
                        fprintf(txt, "ID: %s, x: %f, y: %f,Raio:%f, cor 1: %s, cor 2: %s\n", semaforos->string, semaforos->x, semaforos->y, semaforos->dim1, semaforos->cor1, semaforos->cor2);
                    }
                }
            }
        }
        semaforos = semaforos->next;
    }

    fprintf(txt, "Torres:\n");
    while(torres != NULL){
        if(torres->x - torres->dim1 >= x){
            if(torres->y - torres->dim1 >= y){
                if(torres->x + torres->dim1 <= x + dim1){
                    if(torres->y + torres->dim1 <= y + dim2){
                        //Uh, ela realmente está dentro não é mesmo?
                        fprintf(txt, "ID: %s, x: %f, y: %f, Raio: %f, cor 1: %s, cor 2: %s\n", torres->string, torres->x, torres->y, torres->dim1, torres->cor1, torres->cor2);
                    }
                }
            }
        }
        torres = torres->next;
    }

}

void InsideCirc(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, float x, float y, float dim1){
    fprintf(txt, "Buscando dentro do círculo:\n");
    float dist;
    fprintf(txt, "Quadras:\n");

    while(quadras != NULL){
        dist = sqrt(pow(x - quadras->x ,2) + pow(y - quadras->y ,2));
        if(dist <= dim1){
            dist = sqrt( pow(x - quadras->x, 2) + pow( y - (quadras->y + quadras->dim2) , 2) );
            if(dist <= dim1){
                dist = sqrt( pow(x - (quadras->x + quadras->dim1) , 2) + pow( y - quadras->y , 2));
                if(dist <= dim1){
                    dist = sqrt( pow(x - ( quadras->x + quadras->dim1) ,2) + pow( y - (quadras->y + quadras->dim2 ) ,2) );
                    if(dist <= dim1){
                        //Está dentro
                        fprintf(txt, "CEP: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", quadras->string, quadras->x, quadras->y, quadras->dim1, quadras->dim2, quadras->cor1, quadras->cor2);
                        
                    }
                }
            }
        }
        quadras = quadras->next;
    }

    fprintf(txt, "Hidrantes:\n");

    while(hidrantes != NULL){
        dist = sqrt( pow(x - hidrantes->x , 2) + pow(y - hidrantes->y ,2));
        if(dist + hidrantes->dim1 <= dim1){
            //Está dentro
            fprintf(txt, "ID: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", hidrantes->string, hidrantes->x, hidrantes->y, hidrantes->dim1, hidrantes->dim2, hidrantes->cor1, hidrantes->cor2);
        }
        hidrantes = hidrantes->next;
    }

    fprintf(txt, "Semáforos:\n");

    while(semaforos != NULL){
        dist = sqrt( pow(x - semaforos->x , 2) + pow(y - semaforos->y ,2));
        if(dist + semaforos->dim1 <= dim1){
            //Está dentro
            fprintf(txt, "ID: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", semaforos->string, semaforos->x, semaforos->y, semaforos->dim1, semaforos->dim2, semaforos->cor1, semaforos->cor2);
        }
        semaforos = semaforos->next;
    }

    fprintf(txt, "Torres:\n");

    while(torres != NULL){
        dist = sqrt( pow(x - torres->x , 2) + pow(y - torres->y ,2));
        if(dist + torres->dim1 <= dim1){
            //Está dentro
            fprintf(txt, "ID: %s, x: %f, y: %f, width: %f, height: %f, cor 1: %s, cor 2: %s\n", torres->string, torres->x, torres->y, torres->dim1, torres->dim2, torres->cor1, torres->cor2);
        }
        torres = torres->next;
    }
}

Forma *DeleteQuadrasRect(Forma *quadras, FILE *txt, float x, float y, float dim1, float dim2){
    if(quadras == NULL) return NULL;
    fprintf(txt, "Removendo quadras:\n");
    Forma *aux;
    Forma *aux2;
    //remover primeira quadra se necessário
    while(quadras != NULL){
        if(quadras->x >= x 
        && quadras->y >= y 
        && quadras->x + quadras->dim1 <= x + dim1 
        && quadras->y + quadras->dim2 <= y + dim2){
        
            //Uh, ele realmente está dentro não é mesmo?
            fprintf(txt, "%s removida.\n", quadras->string);
            aux = quadras;                        
            quadras = quadras->next;
            free(aux);
        }
        else
            break;
    }
    if(quadras == NULL) return quadras;
    aux = quadras; aux2 = quadras->next;
    while(aux2 != NULL){
        if(aux2->x >= x 
        && aux2->y >= y 
        && aux2->x + aux2->dim1 <= x + dim1 
        && aux2->y + aux2->dim2 <= y + dim2){
            //Uh, ele realmente está dentro não é mesmo?
            fprintf(txt, "%s removida\n", aux2->string);
            aux->next = aux2->next;
            free(aux2);
            aux2 = aux->next;
            
        }
        else{
            aux = aux->next;
            aux2 = aux2->next;
        }

    }
    return quadras;
}

Forma *DeleteEquipRect(Forma *equip, FILE *txt, float x, float y, float dim1, float dim2){
    if(equip == NULL) return NULL;

    
    Forma *aux;
    Forma *aux2;
    while(equip != NULL){
        if(equip->x - equip->dim1 >= x 
        && equip->y - equip->dim1 >= y 
        && equip->x + equip->dim1 <= x + dim1 
        && equip->y + equip->dim2 <= y + dim2){
            //Está dentro
            fprintf(txt, "%s removido.\n", aux->string);
            aux = equip;
            equip = equip->next;
            free(aux);
        }
        else
            break;
    }
    if(equip == NULL) return NULL;
    aux = equip;
    aux2 = equip->next;
    while(aux2 != NULL){
        if(aux2->x - aux2->dim1 >= x 
        && aux2->y - aux2->dim1 >= y 
        && aux2->x + aux2->dim1 <= x + dim1 
        && aux2->y + aux2->dim2 <= y + dim2){
            //Está dentro
            fprintf(txt, "%s removido.\n", aux->string);
            aux->next = aux2->next;
            free(aux2);
            aux2 = aux->next;
            if(aux2 == NULL) break;
        }
        else{
            aux = aux->next;
            aux2 = aux2->next;
        }
    }
    return equip;
}

Forma *DeleteQuadrasCirc(Forma *quadras, FILE *txt, float x, float y, float dim1){
    if(quadras == NULL) return NULL;
    float dist1, dist2, dist3, dist4;
    fprintf(txt, "Removendo quadras:\n");
    Forma *aux;
    Forma *aux2;

    dist1 = sqrt( pow(x - quadras->x ,2) + pow(y - quadras->y ,2));
    dist2 = sqrt( pow(x - quadras->x, 2) + pow( y - (quadras->y + quadras->dim2) , 2));
    dist3 = sqrt( pow(x - (quadras->x + quadras->dim1) , 2) + pow( y - quadras->y , 2));
    dist4 = sqrt( pow(x - ( quadras->x + quadras->dim1) ,2) + pow( y - (quadras->y + quadras->dim2 ) ,2));
    while(quadras != NULL){
        if(dist1 <= dim1
        && dist2 <= dim1
        && dist3 <= dim1
        && dist4 <= dim1){
            //Está dentro
            fprintf(txt, "%s removida.\n", quadras->string);
            aux = quadras;
            quadras = quadras->next;
            free(aux);
        }
        else
            break;
    }
    if(quadras == NULL) return quadras;
    aux = quadras;
    aux2 = quadras->next;
    while(aux2 != NULL){
        dist1 = sqrt( pow(x - aux2->x ,2) + pow(y - aux2->y ,2));
        dist2 = sqrt( pow(x - aux2->x, 2) + pow( y - (aux2->y + aux2->dim2) , 2) );
        dist3 = sqrt( pow(x - (aux2->x + aux2->dim1) , 2) + pow( y - aux2->y , 2));
        dist4 = sqrt( pow(x - ( aux2->x + aux2->dim1) ,2) + pow( y - (aux2->y + aux2->dim2 ) ,2) );
        
        if(dist1 <= dim1
        && dist2 <= dim1
        && dist3 <= dim1
        && dist4 <= dim1){
            //Está dentro
            fprintf(txt, "%s removida.\n", aux2->string);
            aux->next = aux2->next;
            free(aux2);
            aux2 = aux->next;
            
        }
        else{
            aux = aux->next;
            aux2 = aux2->next;
        }
    }
    return quadras;
}

Forma * DeleteEquipCirc(Forma *equip, FILE *txt, float x, float y, float dim1){
    if(equip == NULL) return NULL;

    Forma *aux;
    Forma *aux2;
    float dist;
    
    while (equip != NULL){
        dist = sqrt( pow(x - equip->x , 2) + pow(y - equip->y, 2));
        if(dist + equip->dim1 <= dim1){
            fprintf(txt, "%s removido.\n", equip->string);//olha esse id
            aux = equip;
            equip = equip->next;
            free(aux);
        }
        else
            break;
    }
    if(equip == NULL) return equip;
    aux = equip;
    aux2 = equip->next;
    while(aux2 != NULL){
        dist = sqrt( pow( x - aux2->x ,2) + pow(y - aux2->y,2) );
        if(dist + aux2->dim1 <= dim1){
            fprintf(txt, "%s removido.\n", aux2->string);
            aux->next = aux2->next;
            free(aux2);
            aux2 = aux->next;
            if(aux2 == NULL) break;
        }
        else{
            aux = aux->next;
            aux2 = aux2->next;
        }
    }
    return equip;
}

void Cchange(Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, char string[], char Cchange1[40], char Cchange2[40]){
    
    while(quadras != NULL){
        if(strcmp(string, quadras->string) == 0){
            strcpy(quadras->cor1, Cchange1);
            strcpy(quadras->cor2, Cchange2);
            return;
        }
        quadras = quadras->next;
    }

    while(hidrantes != NULL){
        if(strcmp(string, hidrantes->string) == 0){
            strcpy(hidrantes->cor1, Cchange1);
            strcpy(hidrantes->cor2, Cchange2);
            return;
        }
        hidrantes = hidrantes->next;
    }

    while(semaforos != NULL){
        if(strcmp(string, semaforos->string) == 0){
            strcpy(semaforos->cor1, Cchange1);
            strcpy(semaforos->cor2, Cchange2);
            return;
        }
        semaforos = semaforos->next;
    }

    while(torres != NULL){
        if(strcmp(string, torres->string) == 0){
            strcpy(torres->cor1, Cchange1);
            strcpy(torres->cor2, Cchange2);
            return;
        }
        torres = torres->next;
    }
}

void Find(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, char string[]){
    while(quadras != NULL){
        if(strcmp(string, quadras->string) == 0){
            fprintf(txt, "Posição da quadra: (&d, %d)", quadras->x, quadras->y);
            return;
        }
        quadras = quadras->next;
    }

    while(hidrantes != NULL){
        if(strcmp(string, hidrantes->string) == 0){
            fprintf(txt, "Posição do hidrante: (&d, %d)", hidrantes->x, hidrantes->y);            
            return;
        }
        hidrantes = hidrantes->next;
    }

    while(semaforos != NULL){
        if(strcmp(string, semaforos->string) == 0){
            fprintf(txt, "Posição do semáforo: (&d, %d)", semaforos->x, semaforos->y);            
            return;
        }
        semaforos = semaforos->next;
    }

    while(torres != NULL){
        if(strcmp(string, torres->string) == 0){
            fprintf(txt, "Posição da torre: (&d, %d)", torres->x, torres->y);            
            return;
        }
        torres = torres->next;
    }

    fprintf(txt, "Não foi encontrado o id ou Cep");
}

int nItems(Forma *inicio){
    int i = 0;
    while(inicio != NULL){
        i++;
        inicio = inicio->next;
    }
    return i;
}

//Usado para menor distância:
void mergesortX(Ponto pontos[], int inicio, int fim){
    if(inicio < fim){
        //int meio = inicio + (fim-1)/2;
        int meio = (inicio + fim)/2;
        mergesortX(pontos, inicio, meio);
        mergesortX(pontos, meio+1, fim);
        mergeX(pontos, inicio, meio, fim);
    }
}

void mergeX(Ponto pontos[], int inicio, int meio, int fim){
    int i, j, k;
    int size1 = meio - inicio + 1;
    int size2 = fim - meio;

    //Criando vetores temporários e copiando-os
    Ponto v1[size1];
    Ponto v2[size2];

    for(i = 0; i < size1; i++){
        strcpy(v1[i].string, pontos[inicio + i].string);
        v1[i].x = pontos[inicio+i].x;
        v1[i].y = pontos[inicio+i].y;
    }
    for(i = 0; i < size2; i++){
        strcpy(v2[i].string, pontos[meio + 1 + i].string);
        v2[i].x = pontos[meio + 1 + i].x;
        v2[i].y = pontos[meio + 1 + i].y;
    }
    i = j = 0;
    k = inicio;
    
    //O verdadeiro merge
    while(i < size1 && j < size2){
        if(v1[i].x <= v2[j].x){
            strcpy(pontos[k].string, v1[i].string);
            pontos[k].x = v1[i].x;
            pontos[k].y = v1[i].y;
            i++;
        }
        else{
            strcpy(pontos[k].string, v2[j].string);
            pontos[k].x = v2[j].x;
            pontos[k].y = v2[j].y;
            j++;
        }
        k++;
    }

    //mandando o que sobrou
    while(i < size1){
        strcpy(pontos[k].string, v1[i].string);
        pontos[k].x = v1[i].x;
        pontos[k].y = v1[i].y;
        i++;
        k++;
    }
    while(j < size2){
        strcpy(pontos[k].string, v2[j].string);
        pontos[k].x = v2[j].x;
        pontos[k].y = v2[j].y;
        j++;
        k++;
    }

}

//Sei que era possível fazer um merge para x e para y em uma só função, porém preferi não fazê-lo
void mergesortY(Ponto pontos[], int inicio, int fim){
    if(inicio < fim){
        //int meio = inicio + (fim-1)/2;
        int meio = (inicio + fim)/2;
        mergesortY(pontos, inicio, meio);
        mergesortY(pontos, meio+1, fim);
        mergeY(pontos, inicio, meio, fim);
    }
}

void mergeY(Ponto pontos[], int inicio, int meio, int fim){
    int i, j, k;

    int size1 = meio - inicio + 1;
    int size2 = fim - meio;

    //Criando vetores temporários e copiando-os
    Ponto v1[size1];
    Ponto v2[size2];

    for(i = 0; i < size1; i++){
        strcpy(v1[i].string, pontos[inicio + i].string);
        v1[i].x = pontos[inicio+i].x;
        v1[i].y = pontos[inicio+i].y;
    }
    for(i = 0; i < size2; i++){
        strcpy(v2[i].string, pontos[meio + 1 + i].string);
        v2[i].x = pontos[meio + 1 + i].x;
        v2[i].y = pontos[meio + 1 + i].y;
    }
    i = j = 0;
    k = inicio;
    
    //O verdadeiro merge
    while(i < size1 && j < size2){
        if(v1[i].y <= v2[j].y){
            strcpy(pontos[k].string, v1[i].string);
            pontos[k].x = v1[i].x;
            pontos[k].y = v1[i].y;
            i++;
        }
        else{
            strcpy(pontos[k].string, v2[j].string);
            pontos[k].x = v2[j].x;
            pontos[k].y = v2[j].y;
            j++;
        }
        k++;
    }

    //mandando o que sobrou
    while(i < size1){
        strcpy(pontos[k].string, v1[i].string);
        pontos[k].x = v1[i].x;
        pontos[k].y = v1[i].y;
        i++;
        k++;
    }
    while(j < size2){
        strcpy(pontos[k].string, v2[j].string);
        pontos[k].x = v2[j].x;
        pontos[k].y = v2[j].y;
        j++;
        k++;
    }

}

float Dist(Ponto a, Ponto b){
    return sqrt( pow(a.x - b.x, 2) + pow(a.y - b.y ,2));
}

float Closest(Ponto pontos[], int nPontos, char *id1, char *id2, float *menorDist){
    //Força bruta se o número de pontos for pequeno
    if(nPontos <= 3){
        float min = FLT_MAX;
        for(int i = 0; i < nPontos; ++i){
            for(int j = i+1; j < nPontos; ++j){
                if(Dist(pontos[i], pontos[j]) < min){
                    min = Dist(pontos[i], pontos[j]);
                    if(min > 0 && min < *menorDist){
                        *menorDist = min;
                        strcpy(id1, pontos[i].string);
                        strcpy(id2, pontos[j].string);
                    }
                }
            }
        }
        return min;
    }

    int mid = nPontos/2;
    Ponto midPoint = pontos[mid];



    //Dividir no meio
    float dEsquerda = Closest(pontos, mid, id1, id2, menorDist);
    float dDireita = Closest(pontos+mid, nPontos - mid, id1, id2, menorDist);

    float dDefinitivo = min(dEsquerda, dDireita);


    //Fazendo uma "tira"
    Ponto tira[nPontos];
    int j = 0;
    for(int i = 0; i < nPontos; i++)
        if(modul(pontos[i].x - midPoint.x) < dDefinitivo){
            tira[j] = pontos[i];
            j++;
        }
    return min(dDefinitivo, TiraPerto(tira, j, dDefinitivo, id1, id2, menorDist));

}

float TiraPerto(Ponto tira[], int nPontos, float dDefinitivo, char *id1, char *id2, float *menorDist){
    //Juro que isso não é n^2
    float min = dDefinitivo;

    mergesortY(tira, 0, nPontos-1);
    for(int i = 0; i < nPontos; ++i){
        for(int j = i+1; j < nPontos && (tira[j].y - tira[i].y) < min; ++j){
            if( Dist(tira[i], tira[j]) < min){
                min = Dist(tira[i], tira[j]);
                if(min < *menorDist){
                    *menorDist = min;
                    strcpy(id1, tira[i].string);
                    strcpy(id2, tira[j].string);

                }
            }
        }
    }
    return min;
}

char *CleanNome(char *nome){
    char *aux;
    int i = strlen(nome) - 1;
    while( i >= 0 && nome[i] != '/' ){
        i--;
    }
    if (i == -1) return nome;


    int n = strlen(nome) - i;
    i++;
    aux = malloc( n*sizeof(char));
    int j = 0;

    while(j < n){
        aux[j] = nome[i];
        i++;
        j++;
    }
    return aux;
}

float modul(float n){
    if(n < 0)
        return -n;
    else
        return n;

}