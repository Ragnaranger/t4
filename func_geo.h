#ifndef func_geo
#define func_geo
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <float.h>

#include "allStruct.h"


void GetChar(FILE *arq, char *str);

void NewForm(int tipo, Forma **formas, float a, float b, float c, float d, char cor1[40], char cor2[40], int id);

void Overlap(FILE *arq, FILE *txt, Forma *formas, Sobreposicao **sobrepor);

void Inside(FILE *arq, FILE *txt, Forma *formas);

void Distance(FILE *arq, char *str, Forma *formas, Reta **retas, FILE *txt);

void NewQuadra(Forma **quadras, char cep[], int x, int y, float dim1, float dim2, char quadraCor1[], char quadraCor2[]);

void NewHidrante(Forma **hidrantes, char id[], int x, int y, char hidranteCor1[], char hidranteCor2[]);

void NewSemaforo(Forma **semaforos, char id[], int x, int y, char semaforoCor1[], char semaforoCor2[]);

void NewTorre(Forma **torres, char id[], int x, int y, char torreCor1[], char torreCor2[]);


#endif