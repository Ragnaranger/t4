#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

typedef struct Cell{
    int n;
    struct Cell *next;
}Cell;


Cell*  newItem(Cell *inicio){
    Cell *aux;
    aux = malloc(sizeof(Cell));
    aux->next = inicio;
    scanf("%d", &(aux->n));
    inicio = aux;
    return inicio;
}


void printList(Cell *inicio){
    printf("\n");
    Cell *aux;
    aux = inicio;
    while(aux != NULL){
        printf("%d ", aux->n);
        aux = aux->next;
    }
    printf("\n");
}

Cell *deleteItem(Cell *inicio){
    if(inicio == NULL) return NULL;
    int val;
    scanf("%d", &val);

    Cell *aux1;
    Cell *aux2;
    while (inicio != NULL){//Caso o primeiro deva ser removido
        if(inicio->n == val){
            aux1 = inicio;
            inicio = inicio->next;
            free(aux1);
        }
        else
            break;

    }
    if(inicio == NULL) return inicio;
    aux1 = inicio;
    aux2 = inicio->next;
    while(aux2 != NULL){//Agora que o primeiro não foi removido, remover o resto
        if(aux2->n == val){
            aux1->next = aux2->next;
            free(aux2);
            aux2 = aux1->next;
            if(aux2 == NULL) break;
        }
        else{
            aux1 = aux1->next;
            aux2 = aux2->next;
        }
    }   

    return inicio;
}


/*int main(){

    Cell *inicio = NULL;
    inicio = newItem(inicio);
    inicio = newItem(inicio);
    inicio = newItem(inicio);
    inicio = newItem(inicio);

    printList(inicio);
    inicio = deleteItem(inicio);
    printList(inicio);

    return 0;
}*/