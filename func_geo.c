#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "func_geo.h"



static float min(float a, float b){
    if(a < b)
        return a;
    else
        return b;
}

static float max(float a, float b){
    if(a > b)
        return a;
    else
        return b;
}

void GetChar(FILE *arq, char *str){
    /*Usada para pegar uma só letra sem o espaço*/
    fscanf(arq, "%s", str);
}

void NewForm(int tipo, Forma **formas, float a, float b, float c, float d, char cor1[40], char cor2[40], int id){
    Forma *aux;
    aux = malloc(sizeof(Forma));
    aux->next = *formas;
    *formas = aux;

    aux->tipo = tipo;
    aux->id = id;
    
    strcpy(aux->cor1, cor1);
    strcpy(aux->cor2, cor2);

    aux->dim1 = a;
    aux->dim2 = b;
    aux->x = c;
    aux->y = d;
}

void Overlap(FILE *arq, FILE *txt, Forma *formas, Sobreposicao **sobrepor){
    int i, j;
    fscanf(arq, "%d %d", &i, &j);
    
    fprintf(txt, "o %d %d\n", i, j);

    Forma *forma1 = formas;
    Forma *forma2 = formas;


    while(forma1->id != i )
        forma1 = forma1->next;
    
    while(forma2->id != j )
        forma2 = forma2->next;
    

    
    if(forma1->tipo == 1 && forma2->tipo == 1){

        if(forma1->x > forma2->x + forma2->dim1 || forma2->x > forma1->x + forma1->dim1 ){
            fprintf(txt, "NAO\n");
        }
        else
        if(forma1->y + forma1->dim2 < forma2->y || forma2->y + forma2->dim2 < forma1->y ){
            fprintf(txt, "NAO\n");
        }
        else{
            fprintf(txt, "SIM\n");

            Sobreposicao *aux;
            aux = malloc(sizeof(Sobreposicao));
            aux->next = *sobrepor;
            *sobrepor = aux;
            aux->x = min( forma1->x , forma2->x );
            aux->y = min( forma1->y , forma2->y );

            aux->xl = max( forma1->x + forma1->dim1 , forma2->x + forma2->dim1) - aux->x;
            aux->yl = max( forma1->y + forma1->dim2 , forma2->y + forma2->dim2) - aux->y;

            
            
        }
    }
    else{
        if(forma1->tipo == 2 && forma2->tipo == 2){
            float dist;
            
            dist = sqrt(pow(forma1->x - forma2->x, 2) + pow( forma1->y - forma2->y, 2));
            if(dist <= forma1->dim1 + forma2->dim1){
                fprintf(txt, "SIM\n");
                Sobreposicao *aux;
                aux = malloc(sizeof(Sobreposicao));
                aux->next = *sobrepor;
                *sobrepor = aux;
                
                
                aux->x = min( forma1->x - forma1->dim1 , forma2->x - forma2->dim1);
                aux->y = min( forma1->y - forma1->dim1 , forma2->y - forma2->dim1);


                aux->xl = max(forma1->x + forma1->dim1 , forma2->x + forma2->dim1) - aux->x;
                aux->yl = max(forma1->y + forma1->dim1 , forma2->y + forma2->dim1) - aux->y;


                
            }
            else
                fprintf(txt, "NAO\n");
            
        }
        else{
            /*As formas são diferentes. Testando a primeira já se sabe a segunda*/
            if(forma1->tipo == 1){
                /*Forma i é o retângulo*/
                /*xperto = max(rect.x, min(circ.x, rect.x+rect.w))*/
                /*yperto = max(rect.y, min(circ.y, rect.y+rect.h))*/
                int xp;
                int yp;

                xp = max( forma1->x, min( forma2->x, forma1->x + forma1->dim1) );
                yp = max( forma1->y, min( forma2->y, forma1->y + forma1->dim2) );
                if(sqrt( pow(xp - forma2->x, 2) + pow(yp - forma2->y, 2) ) <= forma2->dim1  ){
                    fprintf(txt, "SIM\n");

                    Sobreposicao *aux;
                    aux = malloc(sizeof(Sobreposicao));
                    aux->next = *sobrepor;
                    *sobrepor = aux;

                    aux->x = min(forma1->x , forma2->x - forma2->dim1);
                    aux->y = min(forma1->y , forma2->y - forma2->dim1);

                    aux->xl = max(forma1->x + forma1->dim1, forma2->x + forma2->dim1) - aux->x;
                    aux->yl = max(forma1->y + forma1->dim2, forma2->y + forma2->dim1) - aux->y;
                }
                else{
                    fprintf(txt, "NAO\n");
                }
            }
            else{
                /*Forma j é o retângulo*/
                int xp;
                int yp;
                xp = max( forma2->x, min( forma1->x, forma2->x + forma2->dim1) );
                yp = max( forma2->y, min( forma1->y, forma2->y + forma2->dim2) );


                if(sqrt( pow(xp - forma1->x, 2) + pow(yp - forma1->y, 2) ) <= forma1->dim1  ){
                    fprintf(txt, "SIM\n");
                    
                    Sobreposicao *aux;
                    aux = malloc(sizeof(Sobreposicao));
                    aux->next = *sobrepor;
                    *sobrepor = aux;

                    aux->x = min(forma2->x , forma1->x - forma1->dim1);
                    aux->y = min(forma2->y , forma1->y - forma1->dim1);

                    aux->xl = max(forma2->x + forma2->dim1, forma1->x + forma1->dim1) - aux->x;
                    aux->yl = max(forma2->y + forma2->dim2, forma1->y + forma1->dim1) - aux->y;
                    
                }
            }
        }
        
    }

}

void Inside(FILE *arq, FILE *txt, Forma *formas){

    int id;
    float xp;
    float yp;

    fscanf(arq, "%d", &id);

    fscanf(arq, "%f %f", &xp, &yp);

    Forma *aux;
    aux = formas;
    while(aux->id != id)
        aux = aux->next;


    fprintf(txt, "i %d %f %f\n", id, xp, yp);
    if(aux->tipo == 1){
        /*é retângulo*/
        if(aux->x < xp && aux->x + aux->dim1 > xp && aux->y < yp && aux->y + aux->dim2 > yp)
            fprintf(txt, "SIM\n");
        
        else 
            fprintf(txt, "NAO\n");
    }
    else{
        /*é circulo*/
        float dist;
        dist = sqrt(pow(xp - aux->x , 2) + pow( yp - aux->y, 2));
        
        if(dist < aux->dim1)
            fprintf(txt, "SIM\n");
        else
           fprintf(txt, "NAO\n");
    }


}

void Distance(FILE *arq, char *str, Forma *formas, Reta **retas, FILE *txt){
    
    int fig1;
    int fig2;
    float dist;
    GetChar(arq, str);
    fig1 = strtol(str, NULL, 10);
    GetChar(arq, str);
    fig2 = strtol(str, NULL, 10);
    
    Forma *forma1 = formas;
    Forma *forma2 = formas;

    while(forma1->id != fig1)
        forma1 = forma1->next;
    
    while(forma2->id != fig2)
        forma2 = forma2->next;

    fprintf(txt, "d %d %d\n", fig1, fig2);

    if(forma1->tipo == 1 && forma2->tipo == 1){
        /*Dois retângulos*/
        dist = sqrt( pow(forma1->x + forma1->dim1/2 - (forma2->x + forma2->dim1/2 ), 2) + pow(forma1->y + forma1->dim2/2 - (forma2->y + forma2->dim2/2), 2) );
    }
    else if(forma1->tipo == 2 && forma2->tipo == 2){
        /*Dois círculos*/
        dist = sqrt( pow( forma1->x - forma2->x ,2) + pow( forma1->y - forma2->y ,2));
    }
    else if(forma1->tipo == 1){
        /*Retângulo e círculo*/
        dist = sqrt( pow( forma1->x + forma1->dim1/2.0 - forma2->x, 2) + pow(forma1->y + forma1->dim2/2.0 - forma2->y ,2) );
    }
    else{
        /*Círculo e retângulo*/
        dist = sqrt( pow( forma2->x + forma2->dim1/2.0 - forma1->x, 2) + pow(forma2->y + forma2->dim2/2.0 - forma1->y ,2) );
    }

    fprintf(txt, "%f\n", dist);


    Reta *aux;
    aux = malloc(sizeof(Reta));
    aux->next = *retas;
    *retas = aux;


    if(forma1->tipo == 1){
        aux->x1 = forma1->x + (forma1->dim1/2.0);
        aux->y1 = forma1->y + (forma1->dim2/2.0);
    }
    else{
        aux->x1 = forma1->x;
        aux->y1 = forma1->y;
    }

    if(forma2->tipo == 1){
        aux->x2 = forma2->x + (forma2->dim1/2.0);
        aux->y2 = forma2->y + (forma2->dim2/2.0);
    }
    else{
        aux->x2 = forma2->x;
        aux->y2 = forma2->y;
    }
    strcpy(aux->cor, forma1->cor1);


}

void NewQuadra(Forma **quadras, char cep[], int x, int y, float dim1, float dim2, char quadraCor1[], char quadraCor2[]){
    NewForm(3, quadras, dim1, dim2, x, y, quadraCor1, quadraCor2, 3);
    strcpy((*quadras)->string, cep);
}

void NewHidrante(Forma **hidrantes, char id[], int x, int y, char hidranteCor1[], char hidranteCor2[]){
    NewForm(4, hidrantes, 15, 0, x, y, hidranteCor1, hidranteCor2, 0);
    strcpy((*hidrantes)->string, id);
}

void NewSemaforo(Forma **semaforos, char id[], int x, int y, char semaforoCor1[], char semaforoCor2[]){
    NewForm(5, semaforos, 10, 0, x, y, semaforoCor1, semaforoCor2, 0);
    strcpy((*semaforos)->string, id);
}

void NewTorre(Forma **torres, char id[], int x, int y, char torreCor1[], char torreCor2[]){
    NewForm(5, torres, 10, 0, x, y, torreCor1, torreCor2, 0);
    strcpy((*torres)->string, id);
}
