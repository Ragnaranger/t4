#ifndef func_qry
#define func_qry
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <float.h>

#include "allStruct.h"



void InsideRect(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, float x, float y, float dim1, float dim2);

void InsideCirc(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, float x, float y, float dim1);

Forma *DeleteQuadrasRect(Forma *quadras, FILE *txt, float x, float y, float dim1, float dim2);

Forma *DeleteEquipRect(Forma *equip, FILE *txt, float x, float y, float dim1, float dim2);

Forma *DeleteQuadrasCirc(Forma *quadras, FILE *txt, float x, float y, float dim1);

Forma *DeleteEquipCirc(Forma *equip, FILE *txt, float x, float y, float dim1);

void Cchange(Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres,char string[], char Cchange1[40], char Cchange2[40]);

void Find(FILE *txt, Forma *quadras, Forma *hidrantes, Forma *semaforos, Forma *torres, char string[]);

int nItems(Forma *inicio);

void mergesortX(Ponto pontos[], int inicio, int fim);

void mergeX(Ponto pontos[], int inicio, int meio, int fim);

void mergesortY(Ponto pontos[], int inicio, int fim);

void mergeY(Ponto pontos[], int inicio, int meio, int fim);

float Closest(Ponto pontos[], int nPontos, char *id1, char *id2, float *menorDist);

float TiraPerto(Ponto tira[], int nPontos, float dDefinitivo, char *id1, char *id2, float *menorDist);

char *CleanNome(char *nome);

float modul(float n);
#endif