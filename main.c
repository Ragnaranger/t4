#include <stdio.h>
#include <string.h>
#include <float.h>

#include "func_geo.h"
#include "func_qry.h"
#include "Cleaning_main/argTreatment.h"
#include "Cleaning_main/writeSvg.h"



enum ComandoGeo {
    c, r, o, inside, d, a, nx, q, h, s, t, cq, ch, ct, cs, fim
};

static struct {
    char *texto;
    enum ComandoGeo command;
} enum_geo[] = {
    {"c", c},
    {"r", r},
    {"o", o},
    {"i", inside},
    {"d", d},
    {"a", a},
    {"nx", nx},
    {"q", q},
    {"h", h},
    {"s", s},
    {"t", t},
    {"cq", cq},
    {"ch", ch},
    {"ct", ct},
    {"cs", cs},
    {"#", fim}
};


enum ComandoQry {
    qRect, QCirc, dq, dle, Dq, Dle, cc, crd, crb, fim2
};

static struct {
    char *texto;
    enum ComandoQry Qry;
} enum_qry[] = {
    {"q?", qRect},
    {"Q?", QCirc},
    {"dq", dq},
    {"dle", dle},
    {"Dq", Dq},
    {"Dle", Dle},
    {"cc", cc},
    {"crd?", crd},
    {"crb?", crb},
    {"#", fim2},
};


int main(int argc, char *argv[]){
    /*Variáveis para leitura de parâmetros*/
    int nMaxFormas;
    int i;
    int nRetas;
    FILE *entrada;
    FILE *txt;
    
    char str[10];
    int contador;
    char *nomeaux = NULL;
    int figQt;

    //Tratamento de argumentos
    char *path = NULL;//Pasta com arquivos de entrada
    char *file;//Arquivo .geo de entrada
    char *dir;//Pasta com arquivos de saída
    char *qry = NULL;//Nome do arquivo com consultas

    treatArg(argc, argv, &path, &file, &dir, &qry, &nomeaux);

    
    /*Declarar vetores de figura e perguntas*/
    Forma *formas;//Inicio lista ligada formas
    Forma *auxFormas;
    formas = NULL;
    
    nMaxFormas = 1000;/*Número maximo de formas*/
    
    Reta *retas;
    retas = NULL;

    Sobreposicao *sobrepor;
    sobrepor = NULL;

    

    nomeaux = malloc( (strlen(path) + strlen(file) + 1 ) * sizeof(char) );
    nomeaux[0] = '\0';
    strcat(nomeaux, path);
    strcat(nomeaux, file);
    
    /* Abrir arquivo*/
    entrada = fopen(nomeaux, "r");
    if(entrada == NULL){
        printf("Erro na abertura do arquivo\n");
        return 0;
    }

    free(nomeaux);

    char *nome;
    nome = malloc((strlen(file)+1)*sizeof(char) );
    strcpy(nome, file);
    
    /*Tirando .geo do nome*/
    /*int i já declarado*/
    for(i = 0; i < strlen(nome) + 2; i++)
        if( nome[i] == '\0' ){
            nome[i-3] = nome[i-1] = 't';
            nome[i-2] = 'x';
            break;
        }

    /*Criar arquivo txt*/
    nomeaux = malloc( ( strlen(dir) + strlen(nome) + 1) * sizeof(char) );
    nomeaux[0] = '\0';
    strcat(nomeaux, dir);
    strcat(nomeaux, nome);
    txt = fopen(nomeaux ,"a");
    free(nomeaux);
        


    /*Variáveis para criaćão de objetos do T2*/
    int id;
    float x;
    float y;
    float dim1;
    float dim2;
    int tipo;/*1-Retângulo|| 2-Círculo*/
    char cor1[40];
    char cor2[40];

    /*Variáveis para criação de objetos T3*/
    char string[40];//Pode ser usado como cep e identificadores char em geral


    //Inicio de listas ligadas T3:
    Forma *quadras = NULL;
    Forma *hidrantes = NULL;
    Forma *semaforos = NULL;
    Forma *torres = NULL;
    //Cores para estruturas T3:
    /*Cor quadra*/
    char quadraCor1[40];/*Borda*/
    char quadraCor2[40];/*Preencher*/
    /*Cor hidrante*/
    char hidranteCor1[40];
    char hidranteCor2[40];
    /*cor torre*/
    char torreCor1[40];
    char torreCor2[40];
    /*cor semaforo*/
    char semaforoCor1[40];
    char semaforoCor2[40];

    enum ComandoGeo comando;

    
    
    /*--------------------Onde tudo acontece----------------*/
    //Leitura do .geo
    while(strcmp(str, "#") != 0){
        GetChar(entrada, str);


        for(i = 0; i < 16; i++){
            if(!strcmp(str, enum_geo[i].texto)){
                comando = enum_geo[i].command;
                break;
            }
        }

        switch(comando){


            case c:{
                
                fscanf(entrada, "%d %s %s %f %f %f", &id, cor1, cor2, &dim1, &x, &y);

                NewForm(2, &formas, dim1, 0, x, y, cor1, cor2, id);
                break;
            }

            case r:{

                fscanf(entrada, "%d %s %s %f %f %f %f", &id, cor1, cor2, &dim1, &dim2, &x, &y);

                NewForm(1, &formas, dim1, dim2, x, y, cor1, cor2, id);
                break;
            }

            case o:{
                Overlap(entrada, txt, formas, &sobrepor);
                break;
            }

            case inside:{
                Inside(entrada, txt, formas);
                break;
            }

            case d:{
                Distance(entrada, str, formas, &retas, txt);
                break;
            }

            case a:{
                int fig;
                float xp;
                float yp;
                char nomeA[40];
                FILE *a;

                
                fscanf(entrada, "%d", &fig);
                Forma *aux;
                aux = formas;
                while(aux->id != fig)
                aux = aux->next;

                

                if(aux->tipo == 1){
                    xp = aux->x + aux->dim1/2.0;
                    yp = aux->y + aux->dim2/2.0;
                }
                else{

                    xp = aux->x;
                    yp = aux->y;
                }

                fscanf(entrada, "%s", nomeA);
                nomeaux = malloc( ( strlen(dir) + strlen(nomeA) + 4 + 1 ) * sizeof(char));
                nomeaux[0] = '\0';

                strcat(nomeaux, dir);
                strcat(nomeaux, nomeA);
                strcat(nomeaux, ".svg");

                a = fopen(nomeaux, "w");
                free(nomeaux);
                fprintf(a,"<html>\n<body>\n<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100000\" height=\"100000\">\n");
                /*int i;já declarado*/

                aux = formas;

                while(1){

                    if(aux->tipo == 1){
                            fprintf(a, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:%s;stroke:%s;stroke-width:4;opacity:0.6\" />\n", aux->x, aux->y, aux->dim1, aux->dim2, aux->cor2, aux->cor1);
                            fprintf(a, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:purple;stroke-width:2\" />\n", aux->x + aux->dim1/2.0, aux->y + aux->dim2/2.0, xp, yp);
                        }
                        else{
                            fprintf(a, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity = \"0.6\" fill=\"%s\" />\n", aux->x, aux->y, aux->dim1, aux->cor1, aux->cor2);
                            fprintf(a, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:purple;stroke-width:2\" />\n", aux->x, aux->y, xp, yp);
                        }


                    if(aux->next == NULL)
                        break;
                    aux = aux->next;
                }

                fprintf(a, "</svg>\n</body>\n</html>");
                fclose(a);
                break;
            }
            
            case nx:{
                //O comando nx não é mais utilizado pois as figuras são armazenadas em uma lista ligada
                //Ele só está aqui para que casos de teste antigos não causem problemas
                fscanf(entrada, "%d", &nMaxFormas);
                break;
            }

            //Comandos T3:
            case q:{
                fscanf(entrada, "%s %f %f %f %f", string, &x, &y, &dim1, &dim2);
                NewQuadra(&quadras, string, x, y, dim1, dim2, quadraCor1, quadraCor2);
                break;
            }
            case h:{
                fscanf(entrada, "%s %f %f", string, &x, &y);
                NewHidrante(&hidrantes, string, x, y, hidranteCor1, hidranteCor2);
                break;
            }
            case s:{
                fscanf(entrada, "%s %f %f", string, &x, &y);
                NewSemaforo(&semaforos, string, x, y, semaforoCor1, semaforoCor2);
                break;
            }
            case t:{
                fscanf(entrada, "%s %f %f", string, &x, &y);
                NewTorre(&torres, string, x, y, torreCor1, torreCor2);
                break;
            }
            case cq:{
                fscanf(entrada, "%s %s", quadraCor1, quadraCor2);
                break;
            }
            case ch:{
                fscanf(entrada, "%s %s", hidranteCor1, hidranteCor2);
                break;
            }
            case cs:{
                fscanf(entrada, "%s %s", semaforoCor1, semaforoCor2);
                break;
            }
            case ct:{
                fscanf(entrada, "%s %s", torreCor1, torreCor2);
                break;
            }
            case fim:{
                strcpy(str, "#");
                break;
            }
            default:{
                //printf("\nEntrou no default\n");
                strcpy(str, "#");
                break;
            }

        }
    }

    fclose(entrada);
    //Fim da leitura do .geo



    /*--------------------Criar arquivo svg------------------*/
    for(i = 0; i < strlen(nome) + 2; i++)
        if( nome[i] == '\0' ){
            nome[i-3] = 's';
            nome[i-2] = 'v';
            nome[i-1] = 'g';
            break;
        }
    FILE *saida;

    nomeaux = malloc( ( strlen(dir) + strlen(nome) + 1) * sizeof(char) );
    nomeaux[0] = '\0';
    strcat( nomeaux, dir);
    strcat(nomeaux, nome);

    saida = fopen(nomeaux, "w");
    free(nomeaux);

    doSvg(saida, formas, retas, sobrepor, quadras, hidrantes, semaforos, torres);



/*---------------Início de leitura de qry---------------------*/
    if(qry != NULL){
        //printf("Entrando no arquivo qry\n");
        nomeaux = malloc( (strlen(path) + strlen(qry) + 1) * sizeof(char) );
        nomeaux[0] = '\0';
        strcat(nomeaux, path);
        strcat(nomeaux, qry);
        entrada = fopen(nomeaux, "r");
        //printf("\n%s\n", nomeaux);
        free(nomeaux);


        free(nome);
        nome = malloc(sizeof(char) * (strlen(file) + 1));
        strcpy(nome, file);
        char *nome2;
        nome2 = malloc((strlen(qry) + 1) * sizeof(char));
        strcpy(nome2, qry);

        for(i = 0; i < strlen(nome) + 2; i++)
        if( nome[i] == '\0' ){
            nome[i-4] = '\0';
            break;
        }

        nome2 = CleanNome(nome2);

        for(i = 0; i < strlen(nome2) + 2; i++)
        if( nome2[i] == '\0' ){
            nome2[i-4] = '\0';
            break;
        }
        

        nomeaux = malloc( ( strlen(dir) + strlen(nome) + strlen(nome2) + 1 + 5) * sizeof(char) );
        nomeaux[0] = '\0';
        strcat(nomeaux, dir);
        strcat(nomeaux, nome);
        strcat(nomeaux, "-");
        strcat(nomeaux, nome2);
        strcat(nomeaux, ".svg");
        saida = fopen(nomeaux, "w");
        
        fprintf(txt, "\nQry:\n");
        fprintf(saida,"<html>\n<body>\n<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100000\" height=\"100000\">\n");
        enum ComandoQry comandoQry;
    
        /*-----------Onde tudo acontece---------*/
        char Cchange1[40];
        char Cchange2[40];
        
        str[0] = 'a';//Valor aleatorio pra não sair do while precocemente

        while(strcmp(str, "#") != 0){
            strcpy(str, "#");
            GetChar(entrada, str);


            for(i = 0; i < 10; i++){
                if(!strcmp(str, enum_qry[i].texto)){
                    comandoQry = enum_qry[i].Qry;
                    break;
                }
            }
            switch(comandoQry){

                case qRect:{
                    fscanf(entrada, "%f %f %f %f", &x, &y, &dim1, &dim2);
                    InsideRect(txt, quadras, hidrantes, semaforos, torres, x, y, dim1, dim2);
                    fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" stroke-dasharray=\"2,1\" style=\"fill:transparent;stroke:purple;stroke-width:5;opacity:1\" />\n", x, y, dim1, dim2);
                    break;
                }
                case QCirc:{
                    fscanf(entrada, "%f %f %f", &x, &y, &dim1);
                    InsideCirc(txt, quadras, hidrantes, semaforos, torres, x, y, dim1);
                    fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"purple\" stroke-dasharray=\"2,1\" stroke-width=\"4\" opacity=\"0.6\" fill=\"transparent\" />\n", x, y, dim1);
                    break;
                }
                case dq:{
                    fscanf(entrada, "%f %f %f %f", &x, &y, &dim1, &dim2);
                    quadras = DeleteQuadrasRect(quadras, txt, x, y, dim1, dim2);
                    fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" stroke-dasharray=\"2,1\" style=\"fill:transparent;stroke:purple;stroke-width:5;opacity:1\" />\n", x, y, dim1, dim2);
                    break;
                }
                case dle:{
                    fscanf(entrada, "%s %f %f %f %f", string, &x, &y, &dim1, &dim2);
                    fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" stroke-dasharray=\"2,1\" style=\"fill:transparent;stroke:purple;stroke-width:5;opacity:1\" />\n", x, y, dim1, dim2);
                    for(i = 0; i < strlen(string); i++){    
                        if(string[i] == 'r'){
                            fprintf(txt, "Removendo torres:\n");
                            torres = DeleteEquipRect(torres, txt, x, y, dim1, dim2);
                        }
                        else{
                            if(string[i] == 'h'){
                                fprintf(txt, "Removendo hidrantes:\n");
                                hidrantes = DeleteEquipRect(hidrantes, txt, x, y, dim1, dim2);
                            }
                            else{
                                //É semáforo
                                fprintf(txt, "Removendo semáforos:\n");
                                semaforos = DeleteEquipRect(semaforos, txt, x, y, dim1, dim2);
                            }
                        }
                    }
                    break;
                }
                case Dq:{
                    fscanf(entrada, "%f, %f, %f", &dim1, &x, &y);
                    quadras = DeleteQuadrasCirc(quadras, txt, x, y, dim1);
                    fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"purple\" stroke-dasharray=\"2,1\" stroke-width=\"4\" opacity=\"0.6\" fill=\"transparent\" />\n", x, y, dim1);
                    break;
                }
                case Dle:{
                    fscanf(entrada, "%s %f %f %f", string, &x, &y, &dim1);
                    fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"purple\" stroke-dasharray=\"2,1\" stroke-width=\"4\" opacity=\"0.6\" fill=\"transparent\" />\n", x, y, dim1);
                    for(i = 0; i < strlen(string); i++){
                        if(string[i] == 't'){
                            fprintf(txt, "Removendo torres:\n");
                            torres = DeleteEquipCirc(torres, txt, x, y, dim1);
                        }
                        else{
                            if(string[i] == 'h'){
                                fprintf(txt, "Removendo hidrantes:\n");
                                hidrantes = DeleteEquipCirc(hidrantes, txt, x, y, dim1);
                            }
                            else{
                                //É semáforo
                                fprintf(txt, "Removendo semáforos:\n");
                                semaforos = DeleteEquipCirc(semaforos, txt, x, y, dim1);
                            }
                        }
                    }

                    break;
                }
                case cc:{

                    fscanf(entrada, "%s %s %s", string, Cchange1, Cchange2);
                    Cchange(quadras, hidrantes, semaforos, torres, string, Cchange1, Cchange2);
                    break;
                }
                case crd:{
                    fscanf(entrada, "%s", string);
                    Find(txt, quadras, hidrantes, semaforos, torres, string);
                }
                case crb:{
                    int nTorres = nItems(torres);
                    Ponto pontos[nTorres];
                    Forma *aux = torres;
                    for(i = 0; i < nTorres; i++){
                        strcpy(pontos[i].string, aux->string);
                        pontos[i].x = aux->x;
                        pontos[i].y = aux->y;
                        aux = aux->next;
                    }

                    mergesortX(pontos, 0, nTorres-1);
                    i = 0;

                    char id1[40];
                    char id2[40];
                    float menorDist = FLT_MAX;
                    float menorMesmo;

                    //Encontrar pontos
                    menorMesmo = Closest(pontos, nTorres, id1, id2, &menorDist);//Closest depende do merge


                    //Agora só encontrar as torres com os ids
                    fprintf(txt, "Torres mais próximas: %s e %s\nDistância: %f\n", id1, id2, menorMesmo);
                    aux = torres;
                    while(strcmp(aux->string, id1) != 0)
                        aux = aux->next;
                    fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"purple\" stroke-dasharray=\"2,1\" stroke-width=\"4\" opacity=\"0.6\" fill=\"transparent\" />\n", aux->x, aux->y, aux->dim1 + 5);
                    // printf("\nid1: %s\n", id1);
                    // printf("\nid2: %s\n", id2);
                    // printf("menorDist %f\n", menorMesmo);
                    aux = torres;
                    while(strcmp(aux->string, id2) != 0)
                        aux = aux->next;
                    fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"purple\" stroke-dasharray=\"2,1\" stroke-width=\"4\" opacity=\"0.6\" fill=\"transparent\" />\n", aux->x, aux->y, aux->dim1 + 5);
                    
                }
                default:{
                   // printf("Fim qry\n");
                    strcpy(str, "#");
                    break;
                }
            }
        }

        //Agora imprimindo o svg novamente

        doQrySvg(saida, quadras, hidrantes, semaforos, torres);

        fclose(txt);
    }

    return 0;
}