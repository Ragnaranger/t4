#ifndef writeSvg
#define writeSvg

void doSvg(FILE* saida, Forma* formas, Reta* retas, Sobreposicao* sobrepor, Forma* quadras, Forma* hidrantes, Forma* semaforos, Forma* torres);

void doQrySvg(FILE* saida, Forma* quadras, Forma* hidrantes, Forma* semaforos, Forma* torres);
#endif