#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "allStruct.h"
#include "writeSvg.h"

void doSvg(FILE* saida, Forma* formas, Reta* retas, Sobreposicao* sobrepor, Forma* quadras, Forma* hidrantes, Forma* semaforos, Forma* torres){
    fprintf(saida,"<html>\n<body>\n<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100000\" height=\"100000\">\n");
        
        /*----------Formas------------*/
        Forma* auxFormas;
        auxFormas = formas;
        while(1){

            if(auxFormas->tipo == 1)
                fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:%s;stroke:%s;stroke-width:4;opacity:0.6\" />\n", auxFormas->x, auxFormas->y, auxFormas->dim1, auxFormas->dim2, auxFormas->cor2, auxFormas->cor1);

            else
                fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxFormas->x, auxFormas->y, auxFormas->dim1, auxFormas->cor1, auxFormas->cor2);


            if(auxFormas->next == NULL)
                break;
            auxFormas = auxFormas->next;
        }

        /*------------Retas---------------*/

        Reta *auxRetas;
        auxRetas = retas;
        
        while(auxRetas != NULL){
            fprintf(saida, "<line x1=\"%f\" y1=\"%f\" x2=\"%f\" y2=\"%f\" style=\"stroke:%s;stroke-width:2\" />\n",auxRetas->x1, auxRetas->y1, auxRetas->x2, auxRetas->y2, auxRetas->cor);
            fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"purple\">%f</text>",auxRetas->x1 + 10, auxRetas->x2 + 10 ,sqrt( pow(auxRetas->x1 - auxRetas->x2 , 2) + pow( auxRetas->y1 - auxRetas->y2, 2)));

            auxRetas = auxRetas->next;
        }

        /*----------Sobreposições---------*/
        Sobreposicao *auxSobrepor;
        auxSobrepor = sobrepor;
        while(auxSobrepor != NULL){
            fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" stroke-dasharray=\"2,1\" style=\"fill:transparent;stroke:purple;stroke-width:5;opacity:1\" />\n", auxSobrepor->x, auxSobrepor->y, auxSobrepor->xl, auxSobrepor->yl);
            fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"purple\">SOBREPÕE</text>\n", auxSobrepor->x, auxSobrepor->y);

            auxSobrepor = auxSobrepor->next;
        }

        /*-------------Quadras-----------*/
        Forma *auxQuadra;
        auxQuadra = quadras;
        while(auxQuadra != NULL){
            fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:%s;stroke:%s;stroke-width:4;opacity:0.6\" />\n", auxQuadra->x, auxQuadra->y, auxQuadra->dim1, auxQuadra->dim2, auxQuadra->cor2, auxQuadra->cor1);
            auxQuadra = auxQuadra->next;
        }

        /*------------Hidrantes----------*/
        Forma *auxHidrante;
        auxHidrante = hidrantes;
        while(auxHidrante != NULL){
            fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxHidrante->x, auxHidrante->y, auxHidrante->dim1, auxHidrante->cor1, auxHidrante->cor2);
            fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"black\">H</text>", auxHidrante->x - (auxHidrante->dim1 / 2), auxHidrante->y + (auxHidrante->dim1/2));
            auxHidrante = auxHidrante->next;
        }
        /*-------------Semaforo----------*/
        Forma *auxSemaforo;
        auxSemaforo = semaforos;
        while(auxSemaforo != NULL){
            fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxSemaforo->x, auxSemaforo->y, auxSemaforo->dim1, auxSemaforo->cor1, auxSemaforo->cor2);
            auxSemaforo = auxSemaforo->next;
        }

        Forma *auxTorre;
        auxTorre = torres;
        while(auxTorre != NULL){
            fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxTorre->x, auxTorre->y, auxTorre->dim1, auxTorre->cor1, auxTorre->cor2);
            fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"black\">T</text>", auxTorre->x - (auxTorre->dim1/2), auxTorre->y + (auxTorre->dim1/2));
            auxTorre = auxTorre->next;
        }

        fprintf(saida, "</svg>\n</body>\n</html>");
        fclose(saida);
}


void doQrySvg(FILE* saida, Forma* quadras, Forma* hidrantes, Forma* semaforos, Forma* torres){
    
    Forma *auxQuadra;
    auxQuadra = quadras;
    while(auxQuadra != NULL){
        fprintf(saida, "<rect x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" style=\"fill:%s;stroke:%s;stroke-width:4;opacity:0.6\" />\n", auxQuadra->x, auxQuadra->y, auxQuadra->dim1, auxQuadra->dim2, auxQuadra->cor2, auxQuadra->cor1);
        auxQuadra = auxQuadra->next;
    }

    /*------------Hidrantes----------*/
    Forma *auxHidrante;
    auxHidrante = hidrantes;
    while(auxHidrante != NULL){
        fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxHidrante->x, auxHidrante->y, auxHidrante->dim1, auxHidrante->cor1, auxHidrante->cor2);
        fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"black\">H</text>", auxHidrante->x - (auxHidrante->dim1 / 2), auxHidrante->y + (auxHidrante->dim1/2));
        auxHidrante = auxHidrante->next;
    }
    /*-------------Semaforo----------*/
    Forma *auxSemaforo;
    auxSemaforo = semaforos;
    while(auxSemaforo != NULL){
        fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxSemaforo->x, auxSemaforo->y, auxSemaforo->dim1, auxSemaforo->cor1, auxSemaforo->cor2);
        auxSemaforo = auxSemaforo->next;
    }
    /*------------Torre---------------*/
    Forma *auxTorre;
    auxTorre = torres;
    while(auxTorre != NULL){
        fprintf(saida, "<circle cx=\"%f\" cy=\"%f\" r=\"%f\" stroke=\"%s\" stroke-width=\"4\" opacity=\"0.6\" fill=\"%s\" />\n", auxTorre->x, auxTorre->y, auxTorre->dim1, auxTorre->cor1, auxTorre->cor2);
        fprintf(saida, "<text x=\"%f\" y=\"%f\" fill=\"black\">T</text>", auxTorre->x - (auxTorre->dim1/2), auxTorre->y + (auxTorre->dim1/2));
        auxTorre = auxTorre->next;
    }

    fprintf(saida, "</svg>\n</body>\n</html>");
    fclose(saida);
        
    
}

