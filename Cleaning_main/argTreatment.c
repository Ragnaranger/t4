#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "argTreatment.h"

void treatArg(int argc, char *argv[], char** path, char** file, char** dir, char** qry, char** nomeaux){
    /*Variáveis para leitura de parâmetros*/
    int i;

    //Tratamento de argumentos
    // char **path = NULL;//Pasta com arquivos de entrada
    // char **file;//Arquivo .geo de entrada
    // char **dir;//Pasta com arquivos de saída
    // char **qry = NULL;//Nome do arquivo com consultas
    for(i = 1; i < argc; i++){
        if (!strcmp(argv[i], "-f") ){
            i++;
            
            *file = malloc( (strlen(argv[i]) + 1) * sizeof(char));
            strcpy(*file, argv[i]);
        }
        else if(!strcmp(argv[i], "-e") ){
            i++;
            
            *path = malloc((strlen(argv[i]) +1) * sizeof(char));
            strcpy(*path, argv[i]);
        }
        else if(!strcmp(argv[i], "-o")){
            i++;
            *dir = malloc((strlen(argv[i])+1) * sizeof(char));
            strcpy(*dir, argv[i]);
        }
        else if(!strcmp(argv[i], "-q")){
            i++;
            *qry = malloc(( strlen(argv[i]) + 1 ) * sizeof(char));
            strcpy(*qry, argv[i]);
        }
    }

    if(*path == NULL){
        *path = malloc(3 * sizeof(char));
        strcpy(*path, "./");
    }


    /*Tratamento de *path*/
    for(i = 0; i < strlen(*path) + 1; i++){
        if((*path)[i] == '\0')
            if( (*path)[i-1] != '/'){
                (*path)[i] = '/';
                (*path)[i+1] = '\0';
            }
    }
    /*Tratamento *dir*/
    for(i = 0; i < strlen(*dir) + 1; i++){
        if((*dir)[i] == '\0')
            if( (*dir)[i-1] != '/'){
                (*dir)[i] = '/';
                (*dir)[i+1] = '\0';
            }
    }

}