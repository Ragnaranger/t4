#ifndef allStruct
#define allStruct
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <float.h>



typedef struct Forma{
    int id;
    float x;
    float y;
    float dim1;
    float dim2;
    int tipo;/*1-Retângulo|| 2-Círculo || 3-Quadra || 4-Hidrante || 5-Semáforo || 6-Rádio-base*/
    char cor1[40];
    char cor2[40];
    char string[40];//Cep
    struct Forma *next;

}Forma;

typedef struct Reta{
    char cor[35];
    float x1;
    float y1;
    float x2;
    float y2;
    struct Reta *next;
    /*Não é preciso armazenar a distância pois ela pode ser calculada*/
}Reta;

typedef struct Sobreposicao{
    float x;
    float y;
    float xl;
    float yl;
    struct Sobreposicao *next;
}Sobreposicao;

typedef struct Ponto{
    char string[40];
    float x;
    float y;
}Ponto;



#endif