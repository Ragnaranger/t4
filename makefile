COMPILER = gcc
CFLAGS = -lm -std=c99 -pedantic -I. -g

# Colocar aqui os arquivos que devem ser compilados
SRC = $(shell find -type f -name "*.c")
OBJ = $(SRC:%.c=%.o)

siguel: $(OBJ)
	$(COMPILER) -o $@ $^ $(CFLAGS)
	
%.o: %.c
	$(COMPILER) -c -o $@ $< $(CFLAGS)

debug: $(OBJ)
	$(COMPILER) -o siguel $^ $(CFLAGS)

remove:
	rm -rf $(OBJ)